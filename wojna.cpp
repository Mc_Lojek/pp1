﻿#pragma warning(disable:4996)
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <fcntl.h>
#include <io.h>

using namespace std;

#define VAL_JACK 11
#define VAL_QUEEN 12
#define VAL_KING 13
#define VAL_ACE 14

#define SHUFFLE_PRECISSION 500
#define ENDLESS_LIMIT 10000

#define P1_WIN 1
#define P2_WIN 2
#define DRAW 0
#define LOOP -1

#define AMOUNT_SUIT 4 //doesnt change
int AMOUNT_PIP;
int AMOUNT_CARDS;

//kolory kart
enum Suit
{
	DIAMOND,
	HEART,
	CLUB,
	SPADE,
	NONE
}; 

//model karty wartosc i kolor
typedef struct
{
	int val;
	Suit suit;
} Card;

//arrayList do przechowywania kart
typedef struct
{
	Card* data;
	int head;
	int tail;
	int size;
	int rank;
} Queue;

// set all elements at -1 returns false on failure
bool queue_init(Queue* queue)
{
	queue->data = (Card*)malloc(AMOUNT_CARDS * sizeof(Card));
	if (queue->data == NULL)
	{
		return false;
	}
	else if (queue == NULL)
	{
		return false;
	}
	else
	{
		Card empty;
		empty.suit = NONE;
		empty.val = -1;

		for (int i = 0; i < AMOUNT_CARDS; i++)
		{
			queue->data[i] = empty;
		}

		queue->head = 0;
		queue->tail = 0;
		queue->size = 0;
		return true;
	}
}

bool queue_is_empty(Queue* queue)
{
	if (queue->size != 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool queue_is_full(Queue* queue)
{
	if (queue->size >= AMOUNT_CARDS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//add element to queue
bool enqueue(Queue* queue, Card item)
{
	if (queue->size >= AMOUNT_CARDS)
	{
		return false;
	}
	else if(item.val < 2)
	{
		return false;
	}
	else
	{
		queue->data[queue->tail] = item;
		queue->tail++;
		queue->tail %= AMOUNT_CARDS;
		queue->size++;
		return true;
	}
}

//get element from queue
Card dequeue(Queue* queue)
{
	Card item;
	if (queue_is_empty(queue))
	{
		item.suit = NONE;
		item.val = -1;
		return item;
	}
	else
	{
		item = queue->data[queue->head];
		//changing value for empty element
		Card empty;
		empty.suit = NONE;
		empty.val = -1;
		queue->data[queue->head] = empty;

		queue->head++;
		queue->head %= AMOUNT_CARDS;
		queue->size--;

	}
	return item;
}

//transfer elements from one queue to another
bool enqueue_multi(Queue* to, Queue* from)
{
	for (int i = 0; i < from->size;)
	{
		enqueue(to, dequeue(from));
	}

	return true;
}


void deck_show(Card deck[])
{
	for (int i = 0; i < AMOUNT_CARDS; i++)
	{
		cout << deck[i].suit << deck[i].val<<endl;
	}
}

//random order of cards
Card* deck_shuffle(Card deck[])
{
	
	Card temp;
	for (int i = 0; i < SHUFFLE_PRECISSION; i++)
	{
		int random = rand() % AMOUNT_CARDS;
		int index = i % AMOUNT_CARDS;
		temp = deck[index];
		deck[index] = deck[random];
		deck[random] = temp;
	}
	return deck;
}


void deck_init(Card* deck)
{	
	int n = 0;
	for (int i = 0; i < AMOUNT_SUIT; i++)
	{
		Suit s;
		switch (i)
		{
		case 0:
			s = DIAMOND;
			break;
		case 1:
			s = HEART;
			break;
		case 2:
			s = CLUB;
			break;
		case 3:
			s = SPADE;
			break;
		}
		for (int j = VAL_ACE; j > VAL_ACE - AMOUNT_PIP; j--)
		{
			
			deck[n].suit = s;
			deck[n].val = j;
			n++;
		}
	}
}

// give same amount of cards to both players
bool deal(Queue* p1, Queue* p2)
{
	Card* deck = (Card*)malloc(AMOUNT_CARDS * sizeof(Card));
	if (deck)
	{
		p1->rank = 0;
		p2->rank = 0;
		deck_init(deck);
		deck_shuffle(deck);
		//deck_show(deck);
		for (int i = 0; i < AMOUNT_CARDS ; i += 2)
		{
			enqueue(p1, deck[i]);
			if(deck[i].val>= VAL_ACE)
				p1->rank += deck[i].val;
			enqueue(p2, deck[i + 1]);
			if (deck[i+1].val >= VAL_ACE)
				p2->rank += deck[i+1].val;
		}
		free(deck);
		return true;
	}
	else
	{
		return false;
	}
	
}

void show_value(int v)
{
	if (v < 11)
	{
		cout << v;
	}
	else
	{
		switch (v)
		{
			case VAL_ACE: cout << "A"; break;
			case VAL_KING: cout << "K"; break;
			case VAL_QUEEN: cout << "Q"; break;
			case VAL_JACK: cout << "J"; break;
		}
	}
}

void show_suit(Suit s)
{
	_setmode(_fileno(stdout), _O_U16TEXT);
	
	switch (s)
	{
	case DIAMOND: wcout << L"\x2666"; break;
	case HEART: wcout << L"\x2665"; break;
	case CLUB: wcout << L"\x2663"; break;
	case SPADE: wcout << L"\x2660"; break;
	}
	_setmode(_fileno(stdout), _O_TEXT);
}

void show_fight(Card c1, Card c2, int n)
{
	cout << n << ". ";
	show_value(c1.val);
	show_suit(c1.suit);
	cout << " vs ";
	show_value(c2.val);
	show_suit(c2.suit);
	cout << endl;
	
}

//transfer cards from table to winners hand
void return_cards(Queue* hand, Queue* own_table, Queue* enemy_table, Card own_card, Card enemy_card)
{
	enqueue_multi(hand, own_table);
	enqueue(hand, own_card);
	enqueue_multi(hand, enemy_table);
	enqueue(hand, enemy_card);
}

//select player to give him cards
void after_battle(Queue* p1_hand, Queue* p2_hand, Queue* p1_table, Queue* p2_table, Card p1_card, Card p2_card, bool show_state)
{
	if (p1_card.val > p2_card.val) //player 1 wins with enough cards
	{
		if (show_state)
			cout << "   P1 wygral wojne" << endl;
		return_cards(p1_hand, p1_table, p2_table, p1_card, p2_card);
	}
	else if (p1_card.val < p2_card.val) //p2 wins, enough cards
	{
		if (show_state)
			cout << "   P2 wygral wojne" << endl;
		return_cards(p2_hand, p2_table, p1_table, p2_card, p1_card);
	}
	else if (queue_is_empty(p1_hand) && queue_is_empty(p2_hand)) //draw
	{
		//dont return cards
		return;
	}
	else if (queue_is_empty(p2_hand)) //p2 loose end of cards
	{
		if (show_state)
			cout << "   P1 wygral wojne" << endl;
		return_cards(p1_hand, p1_table, p2_table, p1_card, p2_card);
	}
	else if (queue_is_empty(p1_hand)) //p1 looses end of cards
	{
		if (show_state)
			cout << "   P2 wygral wojne" << endl;
		return_cards(p2_hand, p2_table, p1_table, p2_card, p1_card);
	}

	return;
}

void fight(Queue* p1_hand, Queue* p2_hand, Card* p1_card, Card* p2_card, Queue* p1_table, Queue* p2_table, int* n, bool show_state)
{
	bool end_of_cards = false;

	Card empty;
	empty.suit = NONE;
	empty.val = -1;

	enqueue(p1_table, *p1_card);
	enqueue(p2_table, *p2_card);

	*p1_card = empty;
	*p2_card = empty;

	*p1_card = dequeue(p1_hand); //hidden cards
	*p2_card = dequeue(p2_hand); //dont change anything

	if (show_state)
		cout << *n << ". " << "__ vs __" << endl;

	enqueue(p1_table, *p1_card);
	enqueue(p2_table, *p2_card);

	if (queue_is_empty(p1_hand) || queue_is_empty(p2_hand))
	{
		end_of_cards = true;
		n--;
	}

	*p1_card = empty;
	*p2_card = empty;

	*p1_card = dequeue(p1_hand);
	*p2_card = dequeue(p2_hand);

	if (show_state && !end_of_cards)
		show_fight(*p1_card, *p2_card, *n);
}

//wojna wariant A 
void battleA(Queue* p1_hand, Queue* p2_hand, Card p1_card, Card p2_card, int* n, bool show_state)
{
	Queue p1_table;
	Queue p2_table;
	queue_init(&p1_table);
	queue_init(&p2_table);

	Card empty;
	empty.suit = NONE;
	empty.val = -1;

	bool end_of_cards = false;

	do
	{
		(*n)++;
		fight(p1_hand, p2_hand, &p1_card, &p2_card, &p1_table, &p2_table, n, show_state);

	} while ((p1_card.val == p2_card.val) && !queue_is_empty(p1_hand) && !queue_is_empty(p2_hand));

	after_battle(p1_hand, p2_hand, &p1_table, &p2_table, p1_card, p2_card, show_state);

	free(p1_table.data);
	free(p2_table.data);

	return;
}

void borrow_cards(Queue* bigger, Queue* smaller, Queue* b_table, Queue* s_table, Card* b_card, Card* s_card)
{
	enqueue(s_table, *s_card);
	enqueue(b_table, *b_card);
	if (queue_is_empty(smaller))
	{
		enqueue(s_table, dequeue(bigger));
		*s_card = dequeue(bigger);
		enqueue(b_table, dequeue(bigger));
	}
	else if(smaller->size == 1)
	{
		enqueue(s_table, dequeue(smaller));
		enqueue(b_table, dequeue(bigger));
		*s_card = dequeue(bigger);
	}
	
	*b_card = dequeue(bigger);
}

void battleB(Queue* p1_hand, Queue* p2_hand, Card p1_card, Card p2_card, int* n, bool show_state)
{
	Queue p1_table;
	Queue p2_table;
	queue_init(&p1_table);
	queue_init(&p2_table);

	Card empty;
	empty.suit = NONE;
	empty.val = -1;

	int i = 0;

	do
	{
		(*n)++;
		if (queue_is_empty(p1_hand) && p2_hand->size > 3)
		{
			borrow_cards(p2_hand, p1_hand, &p2_table, &p1_table, &p2_card, &p1_card);

			if (show_state)
				cout << *n << ". " << p1_card.val << "vs" << p2_card.val << endl;

			if (p1_card.val <= p2_card.val)
			{
				return_cards(p2_hand, &p2_table, &p1_table, p2_card, p1_card);
			}
			else
			{
				//cout << p1_card.val << "YYYYYYY" << p2_card.val << endl;
				//cout << "pierwszy stracil karty alewygral wojne";
				return_cards(p1_hand, &p1_table, &p2_table, p1_card, p2_card);
			}
			free(p1_table.data);
			free(p2_table.data);
			return;
		}
		else if (queue_is_empty(p2_hand) && p1_hand->size > 3)
		{
			borrow_cards(p1_hand, p2_hand, &p1_table, &p2_table, &p1_card, &p2_card);

			if (p2_card.val <= p1_card.val)
			{
				return_cards(p1_hand, &p1_table, &p2_table, p1_card, p2_card);
			}
			else
			{
				//cout << p1_card.val << "YYYYYYY" << p2_card.val << endl;
				//cout << "drugi stracil karty alewygral wojne";
				return_cards(p2_hand, &p2_table, &p1_table, p2_card, p1_card);
			}
			free(p1_table.data);
			free(p2_table.data);
			return;

		}
		else if (p1_hand->size == 1 && p2_hand->size > 2)
		{
			borrow_cards(p2_hand, p1_hand, &p2_table, &p1_table, &p2_card, &p1_card);


			if (p1_card.val <= p2_card.val)
			{
				return_cards(p2_hand, &p2_table, &p1_table, p2_card, p1_card);
			}
			else
			{
				//cout << p1_card.val << "YYYYYYY" << p2_card.val << endl;
				//cout << "pierwszy stracil karty alewygral wojne";
				return_cards(p1_hand, &p1_table, &p2_table, p1_card, p2_card);
			}
			free(p1_table.data);
			free(p2_table.data);
			return;
		}
		else if (p2_hand->size == 1 && p1_hand->size > 2)
		{
			borrow_cards(p1_hand, p2_hand, &p1_table, &p2_table, &p1_card, &p2_card);

			if (p2_card.val <= p1_card.val)
			{
				return_cards(p1_hand, &p1_table, &p2_table, p1_card, p2_card);
			}
			else
			{
				//cout << p1_card.val << "YYYYYYY" << p2_card.val << endl;
				//cout << "drugi stracil karty alewygral wojne";
				return_cards(p2_hand, &p2_table, &p1_table, p2_card, p1_card);
			}
			free(p1_table.data);
			free(p2_table.data);
			return;
		}
		else 
		{
			fight(p1_hand, p2_hand, &p1_card, &p2_card, &p1_table, &p2_table, n, show_state);
		}

	} while ((p1_card.val == p2_card.val) && !queue_is_empty(p1_hand) && !queue_is_empty(p2_hand));

	after_battle(p1_hand, p2_hand, &p1_table, &p2_table, p1_card, p2_card, show_state);
	free(p1_table.data);
	free(p2_table.data);
}

int compare(int v1, int v2)
{
	if (v1 > v2)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

//rozgrywka
int game(char mode, long long *avg, bool show_state)
{
	Queue p1_hand;
	Queue p2_hand;
	queue_init(&p1_hand);
	queue_init(&p2_hand);

	int end_game = 404;

	deal(&p1_hand, &p2_hand);

	int n = 0;

	while (!queue_is_full(&p1_hand) && !queue_is_full(&p2_hand))
	{
		n++;
		Card p1_card = dequeue(&p1_hand);
		Card p2_card = dequeue(&p2_hand);

		if (show_state)
			show_fight(p1_card, p2_card, n);

		if (p1_card.val > p2_card.val)
		{
			if (show_state)
				cout <<"   P1 wygral bitke"<<endl;
			enqueue(&p1_hand, p1_card);
			enqueue(&p1_hand, p2_card);
		}
		else if (p1_card.val < p2_card.val)
		{
			if (show_state)
				cout << "   P2 wygral bitke"<<endl;
			enqueue(&p2_hand, p2_card);
			enqueue(&p2_hand, p1_card);
			//cout << "";
		}
		else //wojna
		{
			if (show_state)
				cout << "   WOJNA" << endl;

			if (mode == 'a' || mode == 'A')
			{
				battleA(&p1_hand, &p2_hand, p1_card, p2_card, &n, show_state);
			}
			else
			{
				battleB(&p1_hand, &p2_hand, p1_card, p2_card, &n, show_state);
			}
		}

		if (queue_is_empty(&p1_hand) || queue_is_empty(&p2_hand))
		{
			break;
		}
		if (n > ENDLESS_LIMIT) //nieskonczona rozgrywka
		{
			n = 0;

			end_game = LOOP;
			break;
		}
	}

	//cout << "Ruchy:" << n << endl;
	if(avg)
		*avg += n;

	if (n == 0)
	{
		if (show_state)
			cout << endl << "NIESKONCZONA ROZGRYWKA"<<endl;
	}//empty

	else if (queue_is_empty(&p2_hand) && queue_is_full(&p1_hand)) //p1 wins
	{
		if(show_state)
			cout << endl << "KONIEC GRY. WYGRYWA P1"<<endl;
		end_game = P1_WIN;
	}
	else if (queue_is_empty(&p1_hand) && queue_is_full(&p2_hand)) //p2 wins
	{
		if (show_state)
			cout << endl << "KONIEC GRY. WYGRYWA P2"<<endl;
		end_game = P2_WIN;
	}
	else if (queue_is_empty(&p1_hand) && queue_is_empty(&p2_hand)) // remis
	{
		if (show_state)
			cout << endl << "KONIEC GRY. REMIS"<<endl;
		end_game = DRAW;
	}

	/*
	FILE* out;
	if ((out = fopen("korelacja.txt", "a")) == NULL)
	{
		cout << "ERROR nie mozna otworzyc pliku";
	}
	else
	{
		int p1_win = (end_game == P1_WIN) ? 1 : 0;
		int p2_win = (end_game == P2_WIN) ? 1 : 0;
		//fprintf(out, "%i\t%i\t%i\t%i\n", (end_game == P1_WIN) ? 1 : 0, compare(p1_hand.rank, p2_hand.rank), (end_game == P2_WIN) ? 1 : 0, compare(p2_hand.rank, p1_hand.rank));
		fprintf(out, "%i\t%i\t%i\t%i\n", (end_game == P1_WIN) ? 1 : 0, p1_hand.rank, (end_game == P2_WIN) ? 1 : 0, p2_hand.rank);
	}
	fclose(out);
	*/

	free(p1_hand.data);
	free(p2_hand.data);
	return end_game;
}

//simulaate cardgame multiple times
void simulation(int how_many, char mode)
{
	long long avg = 0;
	int result;
	int p1_wins = 0;
	int p2_wins = 0;
	int draw = 0;
	int loop = 0;

	for (int i = 0; i < how_many; i++)
	{
		result = game(mode, &avg, false);
		if (result == DRAW)
		{
			draw++;
		}
		else if (result == P1_WIN)
		{
			p1_wins++;
		}
		else if (result == P2_WIN)
		{
			p2_wins++;
		}
		else if (result == LOOP)
		{
			loop++;
		}
	}
	cout << "Remisy: " << draw << endl
		<< "Pierwszy wygral: " << p1_wins << endl
		<< "Drugi wygral: " << p2_wins << endl
		<< "Nieskonczone: " << loop << endl
		<< "Srednia ilosc ruchow:" << (avg /= (how_many - loop)) << endl;

	/*
	FILE* out;
	if ((out = fopen("daneB.txt", "a")) == NULL)
	{
		cout << "ERROR nie mozna otworzyc pliku";
	}
	else
	{
		fprintf(out, "%i\t%c\t%i\t%i\t%i\t%i\t%i\n", AMOUNT_CARDS, mode, p1_wins, p2_wins, draw, loop, (int)avg);
	}
	fclose(out);
	*/
}

void demo(char mode)
{
	game(mode, NULL, true);
}

int main()
{
	srand(time(NULL));
	char mode;
	int how_many;
	do 
	{
		cout << "Podaj ilosc kart kazdego koloru: ";
		cin >> AMOUNT_PIP;
	} while (AMOUNT_PIP < 5 || AMOUNT_PIP > 13); //od 5 do 13 kart kazdego koloru 
	
	AMOUNT_CARDS = AMOUNT_SUIT * AMOUNT_PIP;

	do
	{
		cout << "Podaj tryb gry (A/B): ";
		cin >> mode;
	} while (mode != 'a' && mode != 'b' && mode != 'A' && mode != 'B');

	cout << "Ile symulacji przeprowadzic: ";
	cin >> how_many;
	simulation(how_many, mode);
	
	//demo(mode);
	
	return 0;
}